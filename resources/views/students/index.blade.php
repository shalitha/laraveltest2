@extends("master")

@section("content")
	<h1>Students</h1>

	<table class="table">
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Options</th>
		</tr>
		@foreach($students as $student)
		<tr>
			<td>{{$student->name}}</td>
			<td>{{$student->email}}</td>
			<td>
				<a href="{{url("students/".$student->id."/edit")}}" class="btn btn-default">Edit</a>
				<a href="{{url("students/".$student->id."/delete")}}" class="btn btn-default">Delete</a>
			</td>
		</tr>
		@endforeach
	</table>
@stop