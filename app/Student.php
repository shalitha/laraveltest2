<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $table = "students";
    //if the table name is the prural of class name, we don't have to specify the 
    //table name
}
